# Battleship

Sorry if I did anything wrong. I tried to resolve the circular dependency between
`GoldVein` and `GoldMiner`; therefore, it's more decoupled.

I made `GoldMiner` the owner of `GoldVien` since `GoldVien` has a weaker connection
to `GoldMiner`. TBH, they don't really need to know each other. All they need
is the ID, and then they can get all the necessary data from `IdleMinerGame`
 (kinda play as the role of the game manager).
