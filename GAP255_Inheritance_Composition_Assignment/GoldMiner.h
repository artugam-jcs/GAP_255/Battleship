#pragma once

class GoldVein;

class GoldMiner
{
private:
    int m_id;
    int m_skill;
    GoldVein* m_pTarget;

public:
    GoldMiner();

    bool IsBusy() { return m_pTarget != nullptr; }

    void Init(int id, int skill);
    void Assign(GoldVein* pVein);
    int Mine();

    void Print();
    void PrintId();

    int GetID();
};