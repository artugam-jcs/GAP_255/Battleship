#include <iostream>

#include "IdleMinerGame.h"

GoldVein::GoldVein()
    : m_id{0}
    , m_quantity{0}
    , m_minerId(-1)
{
}

void GoldVein::Init(int id, int quantity)
{
    m_id = id;
    m_quantity = quantity;
}

void GoldVein::Assign(GoldMiner* pMiner)
{
    m_minerId = pMiner->GetID();
}

int GoldVein::Mine(int quantity)
{
    int output;
    if (m_quantity > quantity)
    {
        m_quantity -= quantity;
        output = quantity;
    }
    else
    {
        output = m_quantity;
        m_quantity = 0;
        std::cout << "Vein " << m_id << " is depleted!\n";
    }
    return output;
}

void GoldVein::Print()
{
    std::cout << "GoldVein " << m_id << " has " << m_quantity << " units of gold";

    if (IsOccupied())
    {
        auto pMiner = IdleMinerGame::GetInstance()->GetMiner(m_id);

        std::cout << ", currently mined by miner ";
        pMiner.PrintId();
    }
}

void GoldVein::PrintId()
{
    std::cout << m_id;
}
