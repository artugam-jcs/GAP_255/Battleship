#include "IdleMinerGame.h"
#include <conio.h>
#include <iostream>
#include "Utility.h"

IdleMinerGame* IdleMinerGame::kInstance = nullptr;

IdleMinerGame* IdleMinerGame::GetInstance()
{
    if (kInstance == nullptr)
        kInstance = new IdleMinerGame();
    return kInstance;
}

IdleMinerGame::IdleMinerGame()
    : m_playerGold{10}
    , m_turnCounter{0}
    , m_goldDiggerCount{0}
    , m_goldVeinCount{0}
{
}

void IdleMinerGame::Play()
{
    while (true)
    {
        system("cls");

        std::cout << "You have " << m_playerGold << " gold.\n";

        // Print miners
        std::cout << "You have " << m_goldDiggerCount << " gold miners.\n";
        for (int i = 0; i < m_goldDiggerCount; ++i)
        {
            m_goldDiggers[i].Print();
            std::cout << '\n';
        }

        // Hiring
        int chanceToHire = Random(1, 6);
        if (m_goldDiggerCount <= 0)
            chanceToHire = 6;
        int minerCost = Random(3, 15);
        if (chanceToHire == 6 && m_goldDiggerCount < 10)
        {
            if (minerCost <= m_playerGold)
            {
                std::cout << "\nPress h to hire a GoldMiner for "
                    << minerCost << " gold.\n\n";
            }
            else
            {
                std::cout << "\nA miner wants " << minerCost
                    << " gold for a job but you can't afford them!\n";
            }
            
        }

        // Veins
        if (m_goldDiggerCount > 0)
        {
            // Finding a new vein
            int chanceToFind = Random(1, 6);
            if (m_goldVeinCount <= 0)
                chanceToFind = 6;
            if (chanceToFind == 6)
            {
                FindMine();
            }

            // Print veins
            std::cout << "You know about " << m_goldVeinCount << " gold veins.\n";
            for (int i = 0; i < m_goldVeinCount; ++i)
            {
                m_goldVeins[i].Print();
                std::cout << '\n';
            }

            // Allow assignment
            if (!AreAllMinersBusy() && !AreAllVeinsOccupied())
            {
                std::cout << "\nPress m to assign a GoldMiner to a vein.\n\n";
            }
        }

        std::cout << "\nPress enter to skip this turn.\n";

        char input = _getch();
        switch(input)
        {
        case 'h':
            if(chanceToHire == 6 && minerCost <= m_playerGold)
                Hire(minerCost);
            break;
        case 'm':
            if (!AreAllMinersBusy() && !AreAllVeinsOccupied())
                Assign();
            break;
        }

        for (int i = 0; i < m_goldDiggerCount; ++i)
        {
            m_playerGold += m_goldDiggers[i].Mine();
        }
    }
}

GoldMiner IdleMinerGame::GetMiner(int id)
{
    return m_goldDiggers[id];
}

bool IdleMinerGame::AreAllMinersBusy()
{
    for (int i = 0; i < m_goldDiggerCount; ++i)
    {
        if (m_goldDiggers[i].IsBusy() == false)
            return false;
    }
    return true;
}

bool IdleMinerGame::AreAllVeinsOccupied()
{
    for (int i = 0; i < m_goldVeinCount; ++i)
    {
        if (m_goldVeins[i].IsOccupied() == false)
            return false;
    }
    return true;
}

void IdleMinerGame::Hire(int cost)
{
    if (m_goldDiggerCount >= 10)
        return;

    m_playerGold -= cost;
    m_goldDiggers[m_goldDiggerCount].Init(m_goldDiggerCount, Random(1,3));
    ++m_goldDiggerCount;
}

void IdleMinerGame::FindMine()
{
    if (m_goldVeinCount >= 10)
        return;

    m_goldVeins[m_goldVeinCount].Init(m_goldVeinCount, Random(1, 25));
    ++m_goldVeinCount;
}

void IdleMinerGame::Assign()
{
    for (int i = 0; i < m_goldDiggerCount; ++i)
    {
        GoldMiner& miner = m_goldDiggers[i];
        if (miner.IsBusy())
            continue;

        std::cout << "Press " << i << " to assign miner " << i << '\n';
    }

    int minerNum;
    std::cin >> minerNum;
    if (minerNum < 0 || minerNum >= m_goldDiggerCount)
    {
        std::cout << "Invalid miner number! You lose a turn.\n";
        system("pause");
        return;
    }

    if (m_goldDiggers[minerNum].IsBusy() == false)
    {
        for (int i = 0; i < m_goldVeinCount; ++i)
        {
            GoldVein& vein = m_goldVeins[i];
            if (vein.IsOccupied())
                continue;

            std::cout << "Press " << i << " to assign to vein " << i << '\n';
        }

        int veinNum;
        std::cin >> veinNum;
        if (veinNum < 0 || veinNum >= m_goldVeinCount)
        {
            std::cout << "Invalid mine number! You lose a turn.\n";
            system("pause");
            return;
        }

        if (m_goldVeins[veinNum].IsOccupied() == false)
        {
            m_goldDiggers[minerNum].Assign(&m_goldVeins[veinNum]);
            m_goldVeins[veinNum].Assign(&m_goldDiggers[minerNum]);
        }
    }
}
