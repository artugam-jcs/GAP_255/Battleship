#include "Utility.h"
#include <random>

int Random(int min, int max)
{
    static bool isSeeded = false;
    if (!isSeeded)
    {
        srand(time(0));
        isSeeded = true;
    }

    return min + rand() % (max - min + 1);
}
