#pragma once

class GoldMiner;

class GoldVein
{
private:
    int m_id;
    int m_quantity;
    int m_minerId;

public:
    GoldVein();

    bool IsOccupied() { return this->m_minerId != -1; }

    void Init(int id, int quantity);
    void Assign(GoldMiner* pMiner);
    int Mine(int quantity);

    void Print();
    void PrintId();
};

