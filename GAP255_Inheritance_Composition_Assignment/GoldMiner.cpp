#include "GoldMiner.h"
#include <iostream>
#include "GoldVein.h"

GoldMiner::GoldMiner()
    : m_id{0}
    , m_skill{0}
    , m_pTarget{nullptr}
{
}

void GoldMiner::Init(int id, int skill)
{
    m_id = id;
    m_skill = skill;
    std::cout << "GoldMiner " << id << " hired.\n";
}

void GoldMiner::Assign(GoldVein* pVein)
{
    m_pTarget = pVein;
}

int GoldMiner::Mine()
{
    if (m_pTarget != nullptr)
    {
        int mined = m_pTarget->Mine(m_skill);
        std::cout << "Miner " << m_id << " mined " << mined << '\n';
        return mined;
    }
    return 0;
}

void GoldMiner::Print()
{
    std::cout << "GoldMiner " << m_id << ", skill: " << m_skill;
    if (m_pTarget != nullptr)
    {
        std::cout << ", currently mining mine ";
        m_pTarget->PrintId();
    }
}

void GoldMiner::PrintId()
{
    std::cout << m_id;
}

int GoldMiner::GetID()
{
    return this->m_id;
}
